package com.example.generics.interface_impl;

import com.example.generics.why.generic.Box;

public class ExtendedGenericBox<U> extends Box<U> {

    @Override
    public void set(U u) {
        System.out.println("Extended functionlity which display to console the value which is et:" + u);
        super.set(u);
    }
}
