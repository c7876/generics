package com.example.generics.interface_impl;

import com.example.generics.why.generic.Box;

class Client {

    public static void main(String[] args) {
        NumericBox numericBox = new NumericBox();
        numericBox.set(1); // here I can only set a number
        numericBox.set(4.5);
        System.out.println(numericBox.isPositive());
        // numericBox.set("abc");// compile error


        Box<Integer> extendedGenericBox = new ExtendedGenericBox<>();
        extendedGenericBox.set(1);

    }
}
