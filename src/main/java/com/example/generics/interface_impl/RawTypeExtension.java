package com.example.generics.interface_impl;

// as we don't specify a type the parameter is default to Object
public class RawTypeExtension implements GenericInterface{
    @Override
    public void generic(Object o) {
        System.out.println(o);
    }
}
