package com.example.generics.interface_impl;

public interface SpecificInterface<T extends Number> {

    void specific(T t);
}
