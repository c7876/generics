package com.example.generics.interface_impl;

public interface GenericInterface<T> {
    void generic(T t);
}
