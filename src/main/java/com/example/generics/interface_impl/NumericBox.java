package com.example.generics.interface_impl;

import com.example.generics.why.generic.Box;
// restrict type at class definition
class NumericBox extends Box<Number> {

    public boolean isPositive() {
        Number number = super.get();
        return number.intValue() > 0;

    }

}
