package com.example.generics.why;


import com.example.generics.why.non_generic.Box;

class Client {

    public static void main(String[] args) {
        final com.example.generics.why.non_generic.Box nonGenericBox = new Box();
        nonGenericBox.set("AString");
        final Object nonGenericBoxValueAsObject = nonGenericBox.get();
        final String nonGenericBoxValueWithExplicitCast = (String) nonGenericBoxValueAsObject;
        System.out.println(nonGenericBoxValueWithExplicitCast);
        nonGenericBox.set(5); // allowed to set a different type

        final com.example.generics.why.generic.Box<String> genericBox = new com.example.generics.why.generic.Box<>();
        genericBox.set("AString");
        final String genericBoxValue = genericBox.get();
        System.out.println(genericBoxValue);
        // genericBox.set(5); // cannot set another type, type safety
    }
}
