package com.example.generics.why.generic;

public class Box<T> {
    // static T type; DOES NOT COMPILE

    // T stands for "Type"
    private T t;

    public void set(T t) { this.t = t; }
    public T get() { return t; }

}
