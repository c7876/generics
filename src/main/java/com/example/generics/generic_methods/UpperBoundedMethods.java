package com.example.generics.generic_methods;

import java.util.ArrayList;
import java.util.List;

public class UpperBoundedMethods {

    public static long total(List<? extends Number> list) {
        long count = 0;
        for (Number number: list)
            count += number.longValue();
        return count;
    }

    public static void main(String[] args) {
        List<? extends Number> list = new ArrayList<Integer>();
        total(list);
    }
}
