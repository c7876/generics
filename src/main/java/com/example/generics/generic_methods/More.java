package com.example.generics.generic_methods;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class More {

   public static <T> void sink(T t) { }
   public static <T> T identity(T t) { return t; }
  // public static T noGood(T t) { return t; } // DOES NOT COMPILE

   // defining multiple types of a method one is the input one is the output
   public static <T, U>  U methodTransform(T input) {
      return null;
   }

   public <T extends Number> void genericMethod(T t) {

   }

   public <T> List<T> fromArrayToList(T[] a) {
      return Arrays.stream(a).collect(Collectors.toList());
   }

   public static <T, G> List<G> fromArrayToList(T[] a, Function<T, G> mapperFunction) {
      return Arrays.stream(a)
              .map(mapperFunction)
              .collect(Collectors.toList());
   }

   // can be declared on instance method as well and they are different type than the one defined(or not) on the class/interface level
   public <X> void instanceMethod() {

   }

   static <T> T pick(T a1, T a2) { return a2; }

//   public <T super Number> void genericMethod2(T t) { Does not compile. Why?
//   }
}
