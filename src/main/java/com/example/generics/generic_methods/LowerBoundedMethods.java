package com.example.generics.generic_methods;

import java.util.ArrayList;
import java.util.List;

public class LowerBoundedMethods {

    public static void addSound(List<? super String> list) {
        list.add("quack");
    }

    public static void main(String[] args) {
        List<String> strings = new ArrayList<String>();
        strings.add("tweet");
        List<Object> objects = new ArrayList<Object>(strings);
        addSound(strings);
        addSound(objects);

        System.out.println(strings);
        System.out.println(objects);
    }
}
