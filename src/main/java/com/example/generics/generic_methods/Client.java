package com.example.generics.generic_methods;

import java.io.Serializable;
import java.util.ArrayList;

class Client {

    public static void main(String[] args) {
        var serializable = More.<Serializable>pick(new String("d"), new ArrayList<String>());

        var object = More.pick(new String("x"), new Object());

    }
}
