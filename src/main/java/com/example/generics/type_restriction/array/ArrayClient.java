package com.example.generics.type_restriction.array;

import java.util.ArrayList;
import java.util.List;

class ArrayClient {

    public static void main(String[] args) {
        final Object[] objects = new Object[4];
        objects[0] = null;
        objects[1] = 1;
        objects[2] = 4.5d;
        objects[3] = "abc";

        Object aNull = objects[0];
        Object anInteger = objects[1];
        Object aDouble = objects[2];
        Object aString = objects[3];

        System.out.println(aNull);
        System.out.println(anInteger);
        System.out.println(aDouble);
        System.out.println(aString);

        // how can I limit the array to hold only integers and doubles?
        // I can use the common class they derive from Number
        Number[] numbers = new Number[3];
        numbers[0] = null;
        numbers[1] = 1;
        numbers[2] = 4.5d;
        // numbers[3] = "abc"; // compiler error as String is not a child of Number

        Number aNullNumber = numbers[0];
        Number anIntegerNumber = numbers[1];
        Number aDoubleNumber = numbers[2];
        System.out.println(aNullNumber);
        System.out.println(anIntegerNumber);
        System.out.println(aDoubleNumber);

        List list = new ArrayList(); // no type specified, so it defaults to Object
        list.add(null);
        list.add(1);
        list.add(4.5d);
        list.add("abc");

        for(Object obj: list){
            System.out.println(obj);
        }
        // no parametrized raw type it is equivalent with this:
        List<Object> objectList = new ArrayList<>();
        objectList.add(null);
        objectList.add(1);
        objectList.add(4.5d);
        objectList.add("abc");

        for(Object obj: objectList){
            System.out.println(obj);
        }
        // how can I limit the list to hold only integers and doubles?
        // I can use the common class they derive from Number
        List<Number> numbersList = new ArrayList<>();
        numbersList.add(null);
        numbersList.add(1);
        numbersList.add(4.5d);
        // numbersList.add("abc");  // compiler error as String is not a child of Number
        for(Number no: numbersList){
            System.out.println(no);
        }

    }
}
