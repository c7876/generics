package com.example.generics.type_erasure.compare;

import java.util.ArrayList;
import java.util.List;

public class GenericsClient {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("abc");
        list.add("xyz");

        for(int i=0; i< list.size(); i++) {
            String obj = list.get(i);
            System.out.println(obj);
        }
    }
}
