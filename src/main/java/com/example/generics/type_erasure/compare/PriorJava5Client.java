package com.example.generics.type_erasure.compare;

import java.util.ArrayList;
import java.util.List;

public class PriorJava5Client {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("abc");
        list.add("xyz");

        for(int i=0; i< list.size(); i++) {
            String obj = (String) list.get(i);
            System.out.println(obj);
        }
    }
}
