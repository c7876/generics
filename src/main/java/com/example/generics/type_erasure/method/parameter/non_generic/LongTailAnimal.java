package com.example.generics.type_erasure.method.parameter.non_generic;

class LongTailAnimal {

    protected void chew(Object input) {}

    //protected void chew(Object input) {} // is already defined
}
