package com.example.generics.type_erasure.method.return_type;

import java.util.List;

public class Goat extends Mammal {
    //public List<String> play() { ... } // DOES NOT COMPILE
    public String sleep() { return ""; }
}
