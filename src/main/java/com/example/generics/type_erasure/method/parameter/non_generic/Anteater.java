package com.example.generics.type_erasure.method.parameter.non_generic;

class Anteater extends LongTailAnimal {

    @Override
    protected void chew(Object input) {}

//    @Override
//    protected void chew(Integer input) {}

      //new method
     protected void chew(Integer input) {}
}
