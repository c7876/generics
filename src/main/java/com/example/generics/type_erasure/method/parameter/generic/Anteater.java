package com.example.generics.type_erasure.method.parameter.generic;

import java.util.ArrayList;
import java.util.List;

public class Anteater extends LongTailAnimal {

//    protected void chew(List<Double> input) {} // DOES NOT COMPILE because of type erasure
    @Override
    protected void chew(List<Object> input) {

    }
    // new method
    protected void chew(ArrayList<Double> input) {}
}
