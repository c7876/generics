package com.example.generics.type_erasure.method.return_type;

import java.util.Collections;
import java.util.List;

class Mammal {
    public List<CharSequence> play() {
        return Collections.emptyList();
    }
    public CharSequence sleep() {
        return new String();
    }
}
