package com.example.generics.type_erasure.method.return_type;

import java.util.ArrayList;

class Monkey extends Mammal {
    public ArrayList<CharSequence> play() {
        return new ArrayList<>();
    }
}
