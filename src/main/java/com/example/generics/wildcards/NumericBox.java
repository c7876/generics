package com.example.generics.wildcards;

class NumericBox<T extends Number>extends com.example.generics.why.generic.Box<T>{

    public boolean isPositive() {
        Number number = super.get();
        return number.intValue() > 0;

    }

    public void set(T t1, T t2) {
        if(t1.intValue() > t2.intValue()) {
            set(t1);
        } else {
            set(t2);
        }
    }
}
