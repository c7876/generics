package com.example.generics.wildcards;

import java.util.ArrayList;
import java.util.List;

class Client {

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        int value = 1;
        integers.add(value);

        List<?> equivalentList = new ArrayList<>();
        equivalentList.add(null);
//        equivalentList.add(1);
//        equivalentList.add(4.5d);
//        equivalentList.add("abc");


        NumericBox<Integer> numericBox = new NumericBox<>();
       // numericBox.set(10, 10.1); DOES NOT Compile (interesting, this is not the same for generic methods)
        numericBox.set(10, 11);
    }
}
