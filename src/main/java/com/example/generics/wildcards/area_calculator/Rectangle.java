package com.example.generics.wildcards.area_calculator;

public class Rectangle  implements Quadrilater{
    private final float length;
    private final float width;

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public float length() {
        return length;
    }

    @Override
    public float width() {
        return width;
    }
}
