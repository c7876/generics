package com.example.generics.wildcards.area_calculator;

class Client {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5, 10);
        Square square = new Square(4);

        QuadrilaterAreaCalculator calculator = new QuadrilaterAreaCalculator();
        System.out.println(calculator.area(rectangle));
        System.out.println(calculator.area(square));
    }
}
