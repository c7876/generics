package com.example.generics.wildcards.area_calculator;

public interface Quadrilater {

    float length();
    float width();

}
