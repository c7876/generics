package com.example.generics.wildcards.area_calculator;

public class QuadrilaterAreaCalculator<T extends Quadrilater> {

    public float area(T rectangle) {
        return rectangle.length() * rectangle.width();
    }
}
