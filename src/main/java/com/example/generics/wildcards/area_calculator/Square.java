package com.example.generics.wildcards.area_calculator;

public class Square implements Quadrilater{
    private final float length;

    public Square(float length) {
        this.length = length;
    }

    @Override
    public float length() {
        return length;
    }

    @Override
    public float width() {
        return length;
    }
}
