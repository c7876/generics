package com.example.generics.questions;

import java.util.ArrayList;
import java.util.List;

public class Questions1 {
    static class A {}
    static class B extends A {}
    static class C extends B {}
    public static void main(String[] args) {
        List<?> list1 = new ArrayList<A>();
        List<? extends A> list2 = new ArrayList<A>();
        List<? super A> list3 = new ArrayList<A>();

       // List<? extends B> list4 = new ArrayList<A>();
    }

//    <B extends A> B third(List<B> list) {
//      //  return new B(); // DOES NOT COMPILE
//        return null;
//    }

    <PARAM_NAME_B extends A> B third(List<B> list) {
        //  return new B(); // DOES NOT COMPILE
        return null;
    }

//    <X> void fifth(List<X super B> list) { // DOES NOT COMPILE
//    }

    <X> void fifth(List<? super B> list) {
    }

}
